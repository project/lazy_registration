Overview
========
The Lazy Registration module captures actions made by anonymous users and stores them against a temporary user account until they are ready to register.
